// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * The {@link TweakableThreadFactory} implements the {@link ThreadFactory} to be
 * used by an {@link ExecutorService}. The {@link TweakableThreadFactory} is
 * providing additional means to set the priority of a {@link Thread} or whether
 * a {@link Thread} is being executed as a daemon {@link Thread} so that the
 * {@link IllegalThreadStateException} instances can be configured as required
 * when being executed by an {@link ExecutorService}.
 * <p>
 * Else there would be no means force {@link Thread} instances to be executed as
 * daemons or with a required priority when executed by the
 * {@link ExecutorService} without such a dedicated {@link ThreadFactory}.
 */
public class TweakableThreadFactory implements ThreadFactory {

	private Boolean _isDaemon = null;
	private int _priority = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link TweakableThreadFactory} with the given attributes.
	 * 
	 * @param aPriority The priority to set for the treads
	 * @param isDaemon True in case the threads are to be executed as daemon
	 *        threads.
	 */
	public TweakableThreadFactory( int aPriority, boolean isDaemon ) {
		_isDaemon = isDaemon;
		_priority = aPriority;
	}

	/**
	 * Constructs the {@link TweakableThreadFactory} with the given attributes.
	 * 
	 * @param aPriority The priority to set for the treads
	 */
	public TweakableThreadFactory( int aPriority ) {
		_priority = aPriority;
	}

	/**
	 * Constructs the {@link TweakableThreadFactory} with the given attributes.
	 * 
	 * @param isDaemon True in case the threads are to be executed as daemon
	 *        threads.
	 */
	public TweakableThreadFactory( boolean isDaemon ) {
		_isDaemon = isDaemon;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Thread newThread( Runnable aRunnable ) {
		final Thread theThread = Executors.defaultThreadFactory().newThread( aRunnable );
		if ( _isDaemon != null ) {
			theThread.setDaemon( _isDaemon );
		}
		if ( _priority != -1 ) {
			theThread.setPriority( _priority );
		}
		return theThread;
	}
}
