// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import org.refcodes.mixin.IndexAccessor;
import org.refcodes.mixin.LengthAccessor;
import org.refcodes.mixin.OffsetAccessor;

/**
 * A {@link Scope} implements the {@link Coupler} interface by determining the
 * validity of an element in the chain by an index relative to a given offset
 * and a given length. E.g. only elements (found at a given index in the chain
 * of elements) within the range of the given length at a given offset are ought
 * to be valid. A length of -1 indicates no length limits at all (e.g. a length
 * of ∞).
 */
public class Scope implements Coupler, OffsetAccessor, LengthAccessor, IndexAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _index;
	private int _offset;
	private int _length;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link Scope} for the given offset and with the given length.
	 * 
	 * @param aOffset The offset for this scope.
	 * @param aLength The length of this scope.
	 */
	public Scope( int aOffset, int aLength ) {
		this( aOffset, aLength, 0 );
	}

	/**
	 * Creates a {@link Scope} with the given offset without any length
	 * limitation.
	 * 
	 * @param aOffset The offset for this scope.
	 */
	public Scope( int aOffset ) {
		this( aOffset, -1, 0 );
	}

	/**
	 * Creates a {@link Scope} for the given offset and with the given length
	 * with the provided index being preset.
	 * 
	 * @param aOffset The offset for this scope.
	 * @param aLength The length of this scope.
	 * @param aIndex The index to be preset.
	 */
	private Scope( int aOffset, int aLength, int aIndex ) {
		if ( aOffset < 0 ) {
			throw new IllegalArgumentException( "The offset <" + aOffset + "> must not be negative." );
		}
		if ( aLength < -1 ) {
			throw new IllegalArgumentException( "The length <" + aLength + "> must either be positive or -1 for infinite length." );
		}
		if ( aIndex < 0 ) {
			throw new IllegalArgumentException( "The index <" + aIndex + "> must not be negative." );
		}
		_offset = aOffset;
		_length = aLength;
		_index = aIndex;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return _length == -1 || _index < _offset + _length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Coupler next() {
		if ( hasNext() ) {
			return new Scope( _offset, _length, _index + 1 );
		}
		throw new IllegalStateException( "The scope with offset <" + _offset + "> anf length <" + _length + ">  has no successor for the current index <" + _index + ">." );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isValid() {
		return _offset <= _index && ( _length == -1 || _index < _offset + _length );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getIndex() {
		return _index;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getOffset() {
		return _offset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [offset=" + _offset + ", length=" + ( _length == -1 ? "∞" : _length ) + ", index = " + _index + "]";
	}
}
