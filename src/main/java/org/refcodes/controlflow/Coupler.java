// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.function.Function;

import org.refcodes.mixin.ValidAccessor;

/**
 * A {@link Coupler} connects (logically) chained elements (linked lists or tree
 * alike structures) together as of conditions being implemented by
 * implementations of this class and determines whether a given element in the
 * chain is valid or not as of the conditions implemented by the
 * {@link Coupler}.
 */
public interface Coupler extends ValidAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUTORS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether this {@link Coupler} has a succeeding {@link Coupler}.
	 * In case we have no following {@link Coupler}, we reached the end of the
	 * chain.
	 * 
	 * @return True in case there is a succeeding {@link Coupler}, false in case
	 *         we reached the end of the chain.
	 */
	boolean hasNext();

	/**
	 * Retrieves the succeeding {@link Coupler}. In case there is no succeeding
	 * {@link Coupler}, then an exception is thrown.
	 * 
	 * @return The succeeding {@link Coupler}.
	 * 
	 * @throws IllegalStateException thrown in case this is the last
	 *         {@link Coupler} in the chain and there is no succeeding coupler.
	 */
	Coupler next();

	/**
	 * Invokes a given operation with the succeeding {@link Coupler} (as of
	 * {@link #next()}) as parameter in case there is a succeeding
	 * {@link Coupler} (as of {@link #hasNext()}) and returns the operation's
	 * result or null in case there is no succeeding {@link Coupler}. The
	 * operation to be invoked must have a single argument being of type
	 * {@link Coupler}.
	 * 
	 * @param <RET> The return type of the operation to be invoked.
	 * @param aOperation The succeeding operation (having a single argument of
	 *        type {@link Coupler}) to be invoked.
	 * 
	 * @return The according result of the invoked operation or null in case
	 *         there is no succeeding {@link Coupler}.
	 */
	default <RET> RET invokeNext( Function<Coupler, RET> aOperation ) {
		return invokeNextOr( aOperation, (RET) null );
	}

	/**
	 * Invokes a given operation with the succeeding {@link Coupler} (as of
	 * {@link #next()}) as parameter in case there is a succeeding
	 * {@link Coupler} (as of {@link #hasNext()}) and returns the operation's
	 * result or the default value in case there is no succeeding
	 * {@link Coupler}. The operation to be invoked must have a single argument
	 * being of type {@link Coupler}.
	 * 
	 * @param <RET> The return type of the operation to be invoked.
	 * @param aSuccessor The succeeding operation (having a single argument of
	 *        type {@link Coupler}) to be invoked.
	 * @param aDefaultValue The default value to be returned in case there is no
	 *        succeeding {@link Coupler}.
	 * 
	 * @return The according result of the invoked operation or the given
	 *         default value in case there is no succeeding {@link Coupler}.
	 */
	default <RET> RET invokeNextOr( Function<Coupler, RET> aSuccessor, RET aDefaultValue ) {
		return hasNext() ? aSuccessor.apply( next() ) : aDefaultValue;
	}

	/**
	 * Invokes a given operation on the given successor with the succeeding
	 * {@link Coupler} (as of {@link #next()}) as parameter in case there is a
	 * succeeding {@link Coupler} (as of {@link #hasNext()}) and returns the
	 * operation's result or null in case there is no succeeding {@link Coupler}
	 * or the successor is null. The operation to be invoked must have a single
	 * argument being of type {@link Coupler}.
	 * 
	 * @param <SUBJECT> The instance which is subject of operation.
	 * @param <RET> The return type of the operation to be invoked.
	 * @param aSuccessor The instance on which to invoke the successor
	 *        operation.
	 * @param aOperation The successor operation (having a single argument of
	 *        type {@link Coupler}) to be invoked.
	 * 
	 * @return The according result of the invoked operation or the given
	 *         default value in case there is no successor or succeeding
	 *         {@link Coupler}.
	 */
	default <SUBJECT, RET> RET invokeNext( SUBJECT aSuccessor, Operation<SUBJECT, Coupler, RET> aOperation ) {
		return invokeNextOr( aSuccessor, aOperation, (RET) null );
	}

	/**
	 * Invokes a given operation on the given successor with the succeeding
	 * {@link Coupler} (as of {@link #next()}) as parameter in case there is a
	 * succeeding {@link Coupler} (as of {@link #hasNext()}) and returns the
	 * operation's result or the default value in case there is no succeeding
	 * {@link Coupler} or the successor is null. The operation to be invoked
	 * must have a single argument being of type {@link Coupler}.
	 * 
	 * @param <SUBJECT> The instance which is subject of operation.
	 * @param <RET> The return type of the operation to be invoked.
	 * @param aSuccessor The instance on which to invoke the successor
	 *        operation.
	 * @param aOperation The successor operation (having a single argument of
	 *        type {@link Coupler}) to be invoked.
	 * @param aDefaultValue The default value to be returned in case there is no
	 *        successor or succeeding {@link Coupler}.
	 * 
	 * @return The according result of the invoked operation or the given
	 *         default value in case there is no successor or succeeding
	 *         {@link Coupler}.
	 */
	default <SUBJECT, RET> RET invokeNextOr( SUBJECT aSuccessor, Operation<SUBJECT, Coupler, RET> aOperation, RET aDefaultValue ) {
		if ( aSuccessor != null && hasNext() ) {
			return aOperation.invoke( aSuccessor, next() );
		}
		return aDefaultValue;
	}

	/**
	 * To be used by an element's method being invoked with a {@link Coupler}
	 * instance as argument belonging to a chain of elements. Returns true in
	 * case the {@link Coupler} is in a valid state for the operation to be
	 * executed. In case {@link #isValid()} returns false, then the operation
	 * should skip execution and pass the succeeding {@link Coupler} as of
	 * {@link #next()} to its successor element in the chain.
	 */
	@Override
	boolean isValid();

	/**
	 * Definition of a method with single argument of type {@link Coupler}.
	 *
	 * @param <SUBJECT> The instance on which to invoke the method.
	 * @param <PARAM> The parameter to be passed to the method.
	 * @param <RET> The return type of the method.
	 */
	public interface Operation<SUBJECT, PARAM extends Coupler, RET> {
		RET invoke( SUBJECT aSubject, PARAM aParam );
	}
}
