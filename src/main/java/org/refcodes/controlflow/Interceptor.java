// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

/**
 * An {@link Interceptor} definition to manufacture a work piece object. This
 * can be achieved by one dedicated interceptor or within several steps.
 *
 * @param <WP> The work piece which is being passed to the implementing
 *        {@link Interceptor} instance(s) to be processed by the responsible
 *        {@link Interceptor} instance(s).
 * @param <R> The return value the {@link Interceptor} type's
 *        {@link #intercept(Object)} method.
 */
@FunctionalInterface
public interface Interceptor<WP, R> {

	/**
	 * This method is invoked with a work as argument. The method processes the
	 * work piece in order to finish the work piece. In case the interceptor is
	 * able to finish the the work piece then this is indicated by returning
	 * true. In case false is returned, then the work piece is considered not to
	 * be finished.
	 *
	 * @param aWorkPiece The work piece which is to be processed by the
	 *        interceptor.
	 * 
	 * @return True in case the interceptor managed to "finish" the work piece.
	 *         In such a case subsequent interceptors may not be required to be
	 *         invoked. In such a case the interceptor signals "finished". In
	 *         case the interceptor did not finish the work piece or is unsure
	 *         whether the work piece is finished, then false is returned.
	 */
	R intercept( WP aWorkPiece );
}
