// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Usually {@link ExecutorService} instances originating from an application
 * server are wrapped with this {@link ManagedExecutorService}.
 * <p>
 * The {@link ManagedExecutorService} wraps an {@link ExecutorService} and
 * prevents execution of the {@link ExecutorService#shutdown()} and
 * {@link ExecutorService#shutdownNow()} methods (them methods are not delegated
 * and therewith disabled).
 * <p>
 * This is helpful when you either expect an {@link ExecutorService} passed by
 * an application server or, if none is passed, you retrieve a private
 * {@link ExecutorService} yourself:
 * <p>
 * In the second case, you might want to shutdown your {@link ExecutorService}
 * after usage, in the first case you do not want to do so as the application
 * server is to take care on shutdown. You then do not wrap your private
 * {@link ExecutorService}; you do wrap any {@link ExecutorService} being passed
 * to you (by an application server).
 * <p>
 * Wrapping the {@link ExecutorService} being passed from the application server
 * and not wrapping the one you create yourself will enable you to shutdown the
 * {@link ExecutorService} in any case; in case it is your private one, it will
 * be shutdown; in case it is the one from the application server, the shutdown
 * calls are just ignored.
 */
public class ManagedExecutorService implements ExecutorService {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ExecutorService _executorService;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new managed executor service.
	 *
	 * @param eExecutorService the e executor service
	 */
	public ManagedExecutorService( ExecutorService eExecutorService ) {
		_executorService = eExecutorService;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean awaitTermination( long aTimeout, TimeUnit aUnit ) throws InterruptedException {
		return _executorService.awaitTermination( aTimeout, aUnit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute( Runnable aArg0 ) {
		_executorService.execute( aArg0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> List<Future<T>> invokeAll( Collection<? extends Callable<T>> aTasks, long aTimeout, TimeUnit aUnit ) throws InterruptedException {
		return _executorService.invokeAll( aTasks, aTimeout, aUnit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> List<Future<T>> invokeAll( Collection<? extends Callable<T>> aTasks ) throws InterruptedException {
		return _executorService.invokeAll( aTasks );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T invokeAny( Collection<? extends Callable<T>> aTasks, long aTimeout, TimeUnit aUnit ) throws InterruptedException, ExecutionException, TimeoutException {
		return _executorService.invokeAny( aTasks, aTimeout, aUnit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T invokeAny( Collection<? extends Callable<T>> aTasks ) throws InterruptedException, ExecutionException {
		return _executorService.invokeAny( aTasks );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isShutdown() {
		return _executorService.isShutdown();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTerminated() {
		return _executorService.isTerminated();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shutdown() {
		/* disabled */
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Runnable> shutdownNow() {
		/* disabled */
		return new ArrayList<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> Future<T> submit( Callable<T> aTask ) {
		return _executorService.submit( aTask );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> Future<T> submit( Runnable aTask, T aResult ) {
		return _executorService.submit( aTask, aResult );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Future<?> submit( Runnable aTask ) {
		return _executorService.submit( aTask );
	}
}
