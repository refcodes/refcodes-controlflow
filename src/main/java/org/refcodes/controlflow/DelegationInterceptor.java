// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

/**
 * A {@link DelegationInterceptor} processes a work piece object in case it is
 * responsible for the work piece, else the work piece is passed to the next
 * (subsequent) {@link DelegationInterceptor} instance.
 *
 * @param <WP> The work piece which is being passed to the implementing
 *        {@link DelegationInterceptor} instance for processing.
 */
@FunctionalInterface
public interface DelegationInterceptor<WP> extends Interceptor<WP, Boolean> {

	/**
	 * This method is invoked with a work piece as argument. The method
	 * processes the work piece if this {@link DelegationInterceptor} is
	 * responsible for the work piece. In case the {@link DelegationInterceptor}
	 * is able to finish the the work piece, then this is indicated by returning
	 * true. In case false is returned, then the work piece is considered not to
	 * be finished and passed to the succeeding {@link DelegationInterceptor}.
	 *
	 * @param aWorkPiece The work piece which is to be processed by the
	 *        {@link DelegationInterceptor} instance.
	 * 
	 * @return True in case the {@link DelegationInterceptor} instance managed
	 *         to finish the work piece. In such a case, subsequent
	 *         {@link DelegationInterceptor} instances are not invoked any more.
	 *         In such a case the true is returned. In case the
	 *         {@link DelegationInterceptor} did not finish the work, then false
	 *         is returned.
	 */
	@Override
	Boolean intercept( WP aWorkPiece );
}
