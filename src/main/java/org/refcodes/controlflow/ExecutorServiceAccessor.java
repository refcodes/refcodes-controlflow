// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.concurrent.ExecutorService;

/**
 * Provides an accessor for a {@link ExecutorService} property.
 */
public interface ExecutorServiceAccessor {

	/**
	 * Retrieves the {@link ExecutorService} from the {@link ExecutorService}
	 * property.
	 * 
	 * @return The {@link ExecutorService} stored by the {@link ExecutorService}
	 *         property.
	 */
	ExecutorService getExecutorService();

	/**
	 * Provides a mutator for a {@link ExecutorService} property.
	 */
	public interface ExecutorServiceMutator {

		/**
		 * Sets the {@link ExecutorService} for the {@link ExecutorService}
		 * property.
		 * 
		 * @param aExecutorService The {@link ExecutorService} to be stored by
		 *        the {@link ExecutorService} property.
		 */
		void setExecutorService( ExecutorService aExecutorService );
	}

	/**
	 * Provides a builder method for a {@link ExecutorService} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ExecutorServiceBuilder<B extends ExecutorServiceBuilder<B>> {

		/**
		 * Sets the {@link ExecutorService} for the {@link ExecutorService}
		 * property.
		 * 
		 * @param aExecutorService The {@link ExecutorService} to be stored by
		 *        the {@link ExecutorService} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withExecutorService( ExecutorService aExecutorService );
	}

	/**
	 * Provides a {@link ExecutorService} property.
	 */
	public interface ExecutorServiceProperty extends ExecutorServiceAccessor, ExecutorServiceMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ExecutorService}
		 * (setter) as of {@link #setExecutorService(ExecutorService)} and
		 * returns the very same value (getter).
		 * 
		 * @param aExecutorService The {@link ExecutorService} to set (via
		 *        {@link #setExecutorService(ExecutorService)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ExecutorService letExecutorService( ExecutorService aExecutorService ) {
			setExecutorService( aExecutorService );
			return aExecutorService;
		}
	}
}
