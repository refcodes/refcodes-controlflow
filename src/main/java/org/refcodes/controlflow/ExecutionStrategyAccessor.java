// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

/**
 * Provides an accessor for an {@link ExecutionStrategy} property.
 */
public interface ExecutionStrategyAccessor {

	/**
	 * Retrieves the execution-strategy from the execution-strategy property.
	 * 
	 * @return The execution-strategy stored by the execution-strategy property.
	 */
	ExecutionStrategy getExecutionStrategy();

	/**
	 * Provides a mutator for an {@link ExecutionStrategy} property.
	 */
	public interface ExecutionStrategyMutator {

		/**
		 * Sets the execution-strategy for the execution-strategy property.
		 * 
		 * @param aExecutionStrategy The execution-strategy to be stored by the
		 *        execution-strategy property.
		 */
		void setExecutionStrategy( ExecutionStrategy aExecutionStrategy );
	}

	/**
	 * Provides an {@link ExecutionStrategy} property.
	 */
	public interface ExecutionStrategyProperty extends ExecutionStrategyAccessor, ExecutionStrategyMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ExecutionStrategy} (setter) as of
		 * {@link #setExecutionStrategy(ExecutionStrategy)} and returns the very
		 * same value (getter).
		 * 
		 * @param aExecutionStrategy The {@link ExecutionStrategy} to set (via
		 *        {@link #setExecutionStrategy(ExecutionStrategy)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ExecutionStrategy letExecutionStrategy( ExecutionStrategy aExecutionStrategy ) {
			setExecutionStrategy( aExecutionStrategy );
			return aExecutionStrategy;
		}
	}
}
