// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

/**
 * The Enum ExecutionStrategy.
 */
public enum ExecutionStrategy {
	/**
	 * The {@link #SEQUENTIAL} {@link ExecutionStrategy} causes state change
	 * requests to be applied on the contained components in sequential order
	 * one after the other. In case an exception occurs, then the exception of
	 * the first erroneous component is forward (thrown). The invoking method
	 * waits till all components have been
	 */
	SEQUENTIAL,
	/**
	 * The {@link #PARALLEL} {@link ExecutionStrategy} causes state change
	 * requests to be applied on the contained components in parallel, each
	 * state change request is applied in its own thread. In case an exception
	 * occurs, then this exception is ignored. The invoking method exits
	 * immediately after all threads for all applicable components have been
	 * started.
	 */
	PARALLEL,
	/**
	 * Similar to {@link #PARALLEL}: The {@link #JOIN} {@link ExecutionStrategy}
	 * causes state change requests to be applied on the contained components in
	 * parallel, each state change request is applied in its own thread. In case
	 * an exception occurs, then the exception of the first erroneous component
	 * is forward (thrown). The invoking method waits till all threads have
	 * executed (died).
	 */
	JOIN

}
