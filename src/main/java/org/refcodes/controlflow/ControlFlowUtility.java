// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.refcodes.data.DaemonLoopSleepTime;
import org.refcodes.data.IoRetryCount;
import org.refcodes.data.Text;
import org.refcodes.mixin.Disposable;

/**
 * Utility class addressing control flow related issues.
 */
public final class ControlFlowUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private ControlFlowUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Code redundancy preventing code snippet (in terms of aspect) throwing an
	 * {@link IllegalStateException} in case an illegal state <code>false</code>
	 * is being provided. This method can be called in the beginning of methods
	 * of instances which can be disposed ({@link Disposable}) or destroyed (as
	 * of <code>org.refcodes.component.Destroyable</code> or decomposed (as of
	 * <code>org.refcodes.component.Decomposeable</code>). In such cases the
	 * "disposed", "destroyed", "decomposed" or similar flag is being passed.
	 * 
	 * @param isIllegalState When true, then an illegal state is being flagged
	 *        which causes the according {@link IllegalStateException} with a
	 *        default message being thrown.
	 * 
	 * @throws IllegalStateException Thrown in case an instance is in a state
	 *         not allowing the invocation of the according method; for calling
	 *         that method the instance is in an illegal state.
	 */
	public static void throwIllegalStateException( boolean isIllegalState ) {
		if ( isIllegalState ) {
			throw new IllegalStateException( Text.ILLEGAL_STATE.getText() );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONCURRENCY:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Joins the {@link Thread} instances contained in the given
	 * {@link Collection}.
	 * 
	 * @param aThreads The {@link Collection} containing the {@link Thread}
	 *        instances to be joined.
	 */
	public static void joinThreads( Collection<? extends Thread> aThreads ) {
		for ( Thread eJoinThread : aThreads ) {
			try {
				eJoinThread.join();
			}
			catch ( InterruptedException ignored ) {}
		}
	}

	/**
	 * Joins the {@link Thread} instances contained in the given
	 * {@link Collection}.
	 * 
	 * @param aThreads The {@link Collection} containing the {@link Thread}
	 *        instances to be joined.
	 */
	public static void joinThreads( Thread... aThreads ) {
		for ( Thread eJoinThread : aThreads ) {
			try {
				eJoinThread.join();
			}
			catch ( InterruptedException ignored ) {}
		}
	}

	/**
	 * Joins the {@link Future} instances contained in the given
	 * {@link Collection}.
	 * 
	 * @param aFutures The {@link Collection} containing the {@link Future}
	 *        instances to be joined.
	 */
	public static void joinFutures( Collection<? extends Future<?>> aFutures ) {
		for ( Future<?> eJoinFuture : aFutures ) {
			try {
				eJoinFuture.get();
			}
			catch ( Exception e ) {
				/* ignore */
			}
		}
	}

	/**
	 * Joins the {@link Future} instances contained in the given
	 * {@link Collection}.
	 * 
	 * @param aFutures The {@link Collection} containing the {@link Future}
	 *        instances to be joined.
	 */
	public static void joinFutures( Future<?>... aFutures ) {
		for ( Future<?> eJoinFuture : aFutures ) {
			try {
				eJoinFuture.get();
			}
			catch ( Exception e ) {
				/* ignore */
			}
		}
	}

	/**
	 * Joins the {@link Future} instances contained in the given
	 * {@link Collection}. Returns the first result detected or throws the first
	 * {@link Exception} which occurred.
	 *
	 * @param aFutures The {@link Collection} containing the {@link Future}
	 *        instances to be joined.
	 * 
	 * @return the object
	 * 
	 * @throws ExecutionException thrown when attempting to retrieve the result
	 *         of a task that aborted by throwing an exception.
	 * @throws InterruptedException Thrown when a thread is waiting, sleeping,
	 *         or otherwise occupied, and the thread is interrupted
	 */
	public static Object waitForFutures( Collection<? extends Future<?>> aFutures ) throws ExecutionException, InterruptedException {
		Exception theException = null;
		Object theResult = null;
		for ( Future<?> eJoinFuture : aFutures ) {
			try {
				if ( theResult == null ) {
					theResult = eJoinFuture.get();
				}
				else {
					eJoinFuture.get();
				}
			}
			catch ( InterruptedException | ExecutionException e ) {
				if ( theException == null ) {
					theException = e;
				}
			}
		}
		if ( theException != null ) {
			if ( theException instanceof ExecutionException ) {
				throw (ExecutionException) theException;
			}
			else if ( theException instanceof InterruptedException ) {
				throw (InterruptedException) theException;
			}
		}
		return theResult;
	}

	/**
	 * Joins the {@link Future} instances contained in the given
	 * {@link Collection}. Returns the first result detected or throws the first
	 * {@link Exception} which occurred.
	 *
	 * @param aFutures The {@link Collection} containing the {@link Future}
	 *        instances to be joined.
	 * 
	 * @return the object
	 * 
	 * @throws Exception the exception
	 */
	public static Object waitForFutures( Future<?>... aFutures ) throws Exception {
		Exception theException = null;
		Object theResult = null;
		for ( Future<?> eJoinFuture : aFutures ) {
			try {
				if ( theResult == null ) {
					theResult = eJoinFuture.get();
				}
				else {
					eJoinFuture.get();
				}
			}
			catch ( Exception e ) {
				if ( theException == null ) {
					theException = e;
				}
			}
		}
		if ( theException != null ) {
			throw theException;
		}
		return theResult;
	}

	/**
	 * Tries to orderly shutdown ({@link ExecutorService#shutdown()}) all tasks
	 * of an {@link ExecutorService}, after a timeout of
	 * {@link IoRetryCount#NORM} milliseconds, shutdown is forced as of
	 * {@link ExecutorService#shutdownNow()}.
	 * -------------------------------------------------------------------------
	 * CAUTION: Do only invoke this methods on {@link ExecutorService} instances
	 * you manage exclusively yourself, do not apply this method on
	 * {@link ExecutorService} instances provided to you by your application
	 * server (e.g. an JEE server).
	 * -------------------------------------------------------------------------
	 * 
	 * @param aExecutorService The service which's threads are to be shut down
	 *        soon.
	 */
	public static void shutdownGracefully( ExecutorService aExecutorService ) {
		shutdownGracefully( aExecutorService, DaemonLoopSleepTime.NORM.getTimeMillis() );
	}

	/**
	 * Tries to orderly shutdown ({@link ExecutorService#shutdown()}) all tasks
	 * of an {@link ExecutorService}, after a given timeout in milliseconds,
	 * shutdown is forced as of {@link ExecutorService#shutdownNow()}. No
	 * shutdown is forced in case a timeout of o is being provided.
	 * -------------------------------------------------------------------------
	 * CAUTION: Do only invoke this methods on {@link ExecutorService} instances
	 * you manage exclusively yourself, do not apply this method on
	 * {@link ExecutorService} instances provided to you by your application
	 * server (e.g. an JEE server).
	 * -------------------------------------------------------------------------
	 * 
	 * @param aExecutorService The service which's threads are to be shut down
	 *        after the given timeout.
	 * @param aTimeoutMillis The timeout after which to force shutdown
	 *        eventually, in case 0 is being provided, then no shutdown is being
	 *        forced (all threads are executed till them terminate normally).
	 */
	public static void shutdownGracefully( ExecutorService aExecutorService, long aTimeoutMillis ) {
		aExecutorService.shutdown();
		if ( aTimeoutMillis > 0 ) {
			try {
				aExecutorService.awaitTermination( DaemonLoopSleepTime.NORM.getTimeMillis(), TimeUnit.MILLISECONDS );
			}
			catch ( InterruptedException ignored ) {}
			aExecutorService.shutdownNow();
		}
	}

	/**
	 * Converts an {@link ExecutorService} to be a
	 * {@link ManagedExecutorService} with methods
	 * {@link ExecutorService#shutdown()} and
	 * {@link ExecutorService#shutdownNow()} being disabled as them are to be
	 * managed by an application server. Usually this method is being invoked
	 * with an {@link ExecutorService} originating from an application server.
	 * See {@link ManagedExecutorService} for more details.
	 * 
	 * @param aExecutorService The {@link ExecutorService} to be transformed to
	 *        a {@link ManagedExecutorService}.
	 * 
	 * @return The {@link ManagedExecutorService} from the provided
	 *         {@link ExecutorService}.
	 */
	public static ExecutorService toManagedExecutorService( ExecutorService aExecutorService ) {
		if ( aExecutorService instanceof ManagedExecutorService ) {
			return aExecutorService;
		}
		return new ManagedExecutorService( aExecutorService );
	}

	// /////////////////////////////////////////////////////////////////////////
	// FACTORIES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns a default {@link ExecutorService } retrieved from the
	 * {@link Executors}.
	 * 
	 * @param isDaemon When true, then daemon threads are created by the
	 *        {@link ExecutorService} returned.
	 * 
	 * @return An {@link ExecutorService} to be used.
	 */
	public static ExecutorService createCachedExecutorService( boolean isDaemon ) {
		if ( isDaemon ) {
			return Executors.newCachedThreadPool( new TweakableThreadFactory( true ) );
		}
		return Executors.newCachedThreadPool();
	}

	/**
	 * Returns a default {@link ExecutorService } retrieved from the
	 * {@link Executors}.
	 * 
	 * @param aCorePoolSize The number of threads to keep in the pool, even if
	 *        they are idle.
	 * @param isDaemon When true, then daemon threads are created by the
	 *        {@link ExecutorService} returned.
	 * 
	 * @return An {@link ScheduledExecutorService} to be used.
	 */
	public static ScheduledExecutorService createScheduledExecutorService( int aCorePoolSize, boolean isDaemon ) {
		if ( isDaemon ) {
			return Executors.newScheduledThreadPool( aCorePoolSize, new TweakableThreadFactory( true ) );
		}
		return Executors.newScheduledThreadPool( aCorePoolSize );
	}
}
