// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

/**
 * The Enum InvocationStrategy.
 */
public enum InvocationStrategy {

	/**
	 * Invoke the next instance. After invocation of the last instance, start
	 * with the first one again. In case an instance cannot be invoked any more,
	 * skip this instance and continue with the next one. Continue till all
	 * instances cannot be invoked any more.
	 */
	ROUND_ROBIN,
	/**
	 * Start with the first instance and invoke it till it cannot be invoked any
	 * more. Use the next instance till it also cannot be invoked any more.
	 * Continue with this strategy, invocation finishes with the last instance
	 * till cannot be invoked any more.
	 */
	FIRST_TO_LAST,
	/**
	 * Start with the last instance and invoke it till it cannot be invoked any
	 * more. Use the preceding instance till it also cannot be invoked any mire.
	 * Continue with this strategy, invocation finishes with the first instance
	 * till it cannot be invoked any more.
	 */
	LAST_TO_FIRST;

}
