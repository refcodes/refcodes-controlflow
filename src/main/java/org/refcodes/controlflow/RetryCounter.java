// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import org.refcodes.mixin.RetryCountAccessor;
import org.refcodes.mixin.RetryNumberAccessor;

/**
 * The {@link RetryCounter} can be used in loops to test whether a retry should
 * take place and in case a retry is to take place, then a given period of time
 * (delay) is waited and the internal retry counter is decremented. In case no
 * more retries are "available" the business logic may act accordingly such as
 * throwing an exception.
 * <p>
 * In case a delay greater than 0 is set, then each retry is being delayed by
 * the according amount of milliseconds, in case no retries are left, then
 * delaying is skipped. A delay of 0 or smaller skips delaying altogether.
 * <p>
 * The first call to {@link #nextRetry()} does not delay as the first call is
 * not considered to be a retry. In case an exponential retry delay extension
 * time has been provided, then the retry delay upon calling
 * {@link #nextRetry()} is extended by that exponential delay (being increased
 * for each retry).
 * <p>
 * ATTENTION: The first call the {@link #nextRetry()} increments the retry
 * counter {@link #getRetryCount()} to be 1 and not 0, so the retry counter is
 * actually a "try" counter, having the first "try" upon invoking
 * {@link #nextRetry()}.
 */
public class RetryCounter implements Retryable, RetryNumberAccessor, RetryCountAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _retryNumber;
	private long _retryDelayMillis;
	private long _exponentialRetryDelayExtensionMillis;
	private int _retryCount = 0;
	private long _currentRetryDelayMillis = 0;
	private int _totalRetryDelayMillis = 0;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a retry retry counter by providing the number of retries with
	 * no delay before each retry.
	 * 
	 * @param aRetryNumber Sets the number of retires this instance allows
	 *        altogether
	 */
	public RetryCounter( int aRetryNumber ) {
		this( aRetryNumber, -1L, 0L );
	}

	/**
	 * Constructs a retry timeout counter by providing the total delay time and
	 * the delay before each retry. The total delay time is the total time this
	 * {@link RetryCounter} will delay till the last call of the
	 * {@link #nextRetry()}. It does not take the time "lost" into account lost
	 * by business logic calling the {@link RetryCounter}. The actual calculated
	 * number of retires may be a rounded one, depending whether the total delay
	 * divided by the retry delay has a reminder. Basically speaking this is a
	 * convenience constructor calling
	 * {@link RetryCounter#RetryCounter(int, long, long)} calculating the number
	 * of retires by:
	 * -------------------------------------------------------------------------
	 * "aRetryNumber = (aTotalDelayMillis / aRetryDelayMillis)"
	 * -------------------------------------------------------------------------
	 * 
	 * @param aTotalDelayMillis Sets the total delay time this instance allows
	 *        altogether. From this number divided by the actual retry delay
	 *        results in the number of retires.
	 * @param aRetryDelayMillis The delay before each retry to wait in
	 *        milliseconds. When less than or equal to 0, then the delay is
	 *        skipped.
	 */
	public RetryCounter( long aTotalDelayMillis, long aRetryDelayMillis ) {
		this( (int) ( aTotalDelayMillis / aRetryDelayMillis ), aRetryDelayMillis, 0L );
	}

	/**
	 * Constructs a retry timeout counter by providing the number of retries and
	 * the delay before each retry. In case a delay greater than 0 is set, then
	 * each retry is being delayed by the according amount of milliseconds, in
	 * case no retries are left, then delaying is skipped. A delay of 0 or
	 * smaller skips delaying altogether.
	 * 
	 * @param aRetryNumber Sets the number of retires this instance allows
	 *        altogether (value of -1 represents an infinite number which can
	 *        only be aborted via {@link #abort()}).
	 * @param aRetryDelayMillis The delay before each retry to wait in
	 *        milliseconds. When less than or equal to 0, then the delay is
	 *        skipped.
	 */
	public RetryCounter( int aRetryNumber, long aRetryDelayMillis ) {
		this( aRetryNumber, aRetryDelayMillis, 0L );
	}

	/**
	 * Constructs a retry timeout counter by providing the number of retries and
	 * the delay before each retry and taking an exponential delay extension
	 * into calculation:
	 * -------------------------------------------------------------------------
	 * "(getRetryDelayMillis() + (Math.pow( 4, getRetryCount() - 1 ) *
	 * getExpRetryDelayExtensionMillis())"
	 * -------------------------------------------------------------------------
	 * In case a delay greater than 0 is set, then each retry is being delayed
	 * by the according amount of milliseconds, in case no retries are left,
	 * then delaying is skipped. In case an exponential delay extension is being
	 * set greater than 0, then the delay is extended accordingly. A delay of 0
	 * or smaller skips delaying altogether.
	 *
	 * @param aRetryNumber Sets the number of retires this instance allows
	 *        altogether
	 * @param aRetryDelayMillis The delay before each retry to wait in
	 *        milliseconds. When less than or equal to 0, then the delay is
	 *        skipped.
	 * @param aExpRetryDelayExtensionMillis the exponential retry delay
	 *        extension in milliseconds
	 */
	public RetryCounter( int aRetryNumber, long aRetryDelayMillis, long aExpRetryDelayExtensionMillis ) {
		_retryNumber = aRetryNumber;
		_retryDelayMillis = aRetryDelayMillis;
		_exponentialRetryDelayExtensionMillis = aExpRetryDelayExtensionMillis;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Tests whether a next retry is possible. In case this is true, then the
	 * retry count is incremented by one. In case this is not the first call to
	 * the retry counter, then the thread is delayed by the configured delay
	 * time. In case
	 * -------------------------------------------------------------------------
	 * The first call does not delay as the first call is not considered to be a
	 * retry. In case an exponential retry delay extension time has been
	 * provided, then the retry delay is extended by that exponential delay
	 * being increased for each retry.
	 * -------------------------------------------------------------------------
	 * ATTENTION: The first call the {@link #nextRetry()} increments the retry
	 * counter {@link #getRetryCount()} to be 1 and not 0, so the retry counter
	 * is actually a "try" counter, having the first "try" upon invoking
	 * {@link #nextRetry()}.
	 * -------------------------------------------------------------------------
	 * 
	 * @return True in case there is a next retry as of {@link #hasNextRetry()}.
	 */
	@Override
	public boolean nextRetry() {
		if ( hasNextRetry() ) {
			if ( _retryCount != 0 ) {
				_currentRetryDelayMillis = getNextRetryDelayMillis();
				if ( _currentRetryDelayMillis > 0 ) {
					synchronized ( this ) {
						try {
							Thread.sleep( _currentRetryDelayMillis );
							_totalRetryDelayMillis += _currentRetryDelayMillis;
						}
						catch ( InterruptedException ignored ) {}
					}
				}
			}
			if ( _retryNumber != -1 ) {
				_retryCount++;
			}
			return true;
		}
		_currentRetryDelayMillis = 0;
		return false;
	}

	/**
	 * Calculates the next retry delay in milliseconds. This is the time the
	 * {@link #nextRetry()} method will wait in case its rules match for doing a
	 * sleep.
	 * 
	 * @return The next retry delay in milliseconds.
	 */
	public long getNextRetryDelayMillis() {
		if ( !hasNextRetry() || _retryNumber == 1 ) {
			return 0;
		}
		long theCurrentRetryDelayMillis = _currentRetryDelayMillis;
		if ( _exponentialRetryDelayExtensionMillis > 0 && ( _retryCount > 1 || getInitialRetryDelayMillis() == 0 ) ) {
			theCurrentRetryDelayMillis = (long) ( _retryDelayMillis + ( Math.pow( 4, _retryCount - 1 ) * _exponentialRetryDelayExtensionMillis ) );
		}
		else {
			theCurrentRetryDelayMillis = _retryDelayMillis;
		}
		return theCurrentRetryDelayMillis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNextRetry() {
		return ( _retryCount < _retryNumber || ( _retryNumber == -1 && _retryCount != -1 ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void restart() {
		_retryCount = 0;
		_currentRetryDelayMillis = 0;
		_totalRetryDelayMillis = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void abort() {
		_retryCount = _retryNumber;
	}

	// /////////////////////////////////////////////////////////////////////////
	// ATTRIBUTES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the number of retires this instance allows altogether.
	 * 
	 * @return The number of altogether retries.
	 */
	@Override
	public int getRetryNumber() {
		return _retryNumber;
	}

	/**
	 * Returns the initial delay before each retry to wait in milliseconds. The
	 * {@link #getCurrentRetryDelayMillis()} is calculated from the initial
	 * delay and the exponential retry delay added for each retry (not added for
	 * the first call to {@link #nextRetry()}).
	 * 
	 * @return The delay to being set in milliseconds.
	 */
	public long getInitialRetryDelayMillis() {
		return _retryDelayMillis;
	}

	/**
	 * Returns the current delay before a retry to wait in milliseconds. When
	 * less than or equal to 0, then the delay is skipped. The current delay is
	 * calculated by extending the {@link #getInitialRetryDelayMillis()} with
	 * the {@link #getExponentialRetryDelayExtensionMillis()} per retry.
	 * -------------------------------------------------------------------------
	 * The current retry delay gets incremented for each retry by an exponential
	 * retry delay extension time calculated from in case it is greater than 0.
	 * -------------------------------------------------------------------------
	 * 
	 * @return The delay to being set in milliseconds.
	 */
	public long getCurrentRetryDelayMillis() {
		return _currentRetryDelayMillis;
	}

	/**
	 * Returns the exponential retry delay extension time in milliseconds. In
	 * case an exponential retry delay extension time has been provided, then
	 * the retry delay upon calling {@link #nextRetry()} is extended by that
	 * exponential delay (being increased for each retry):
	 * -------------------------------------------------------------------------
	 * "(getRetryDelayMillis() + (Math.pow( 4, getRetryCount() - 1 ) *
	 * getExpRetryDelayExtensionMillis())"
	 * -------------------------------------------------------------------------
	 * 
	 * @return The exponential retry delay extension time in milliseconds.
	 */
	public long getExponentialRetryDelayExtensionMillis() {
		return _exponentialRetryDelayExtensionMillis;
	}

	/**
	 * The current state regarding the retires. It specifies at which retry we
	 * currently are.
	 * -------------------------------------------------------------------------
	 * ATTENTION: The first call the {@link #nextRetry()} increments the retry
	 * counter {@link #getRetryCount()} to be 1 and not 0, so the retry counter
	 * is actually a "try" counter, having the first "try" upon invoking
	 * {@link #nextRetry()}.
	 * -------------------------------------------------------------------------
	 * 
	 * @return The number of retries used up so far
	 */
	@Override
	public int getRetryCount() {
		return _retryCount;
	}

	// /////////////////////////////////////////////////////////////////////////
	// IMPLEMENTATION SPECIFIC:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the total retry delay already accumulated as of calling the
	 * {@link #nextRetry()} method since the initial construction or the last
	 * {@link #restart()}.
	 * 
	 * @return The accumulated total delay already taken effect.
	 */
	public long getTotalRetryDelayMillis() {
		return _totalRetryDelayMillis;
	}

	/**
	 * Sets the delay before each retry to wait in milliseconds. When less than
	 * or equal to 0, then the delay is skipped.
	 * 
	 * @param aRetryDelayMillis The delay to be set in milliseconds.
	 */
	public void setRetryDelayMillis( long aRetryDelayMillis ) {
		_retryDelayMillis = aRetryDelayMillis;
	}
}
