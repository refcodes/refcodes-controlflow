// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.concurrent.atomic.AtomicInteger;

import org.refcodes.data.SleepLoopTime;
import org.refcodes.exception.Exceptional;
import org.refcodes.mixin.Disposable;
import org.refcodes.mixin.Releaseable;

/**
 * The {@link ExceptionWatchdog} implements a plain {@link ExceptionWatchdog}
 * not queuing any {@link Exception} instances. Just the {@link Exception}
 * instances thrown via {@link #throwException(Exception)} are caught in case
 * there is an awaiting thread inside the {@link #catchException()} method.
 * <p>
 * ATTENTION: This implementation is not 100% reliable whether very fast
 * sequenced {@link Exception} instances fed to
 * {@link #throwException(Exception)} are all caught by threads awaiting the
 * {@link #catchException()} method calls! In cases you expect one exception and
 * then "its over", this class does its job.
 *
 * @param <E> the element type
 */
public class ExceptionWatchdog<E extends Exception> implements Exceptional<E>, Disposable, Releaseable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	AtomicInteger _threadCount = new AtomicInteger( 0 );
	private E _exception = null;
	private E _lastException = null;
	boolean _isDisposed = false;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This methods awaits an {@link Exception}. It waits (sleeps) until an
	 * {@link Exception} arises via {@link #throwException(Exception)} which is
	 * then thrown by this {@link #catchException()} method. In case
	 * {@link #releaseAll()} is being called, then all waiting threads are
	 * continued without throwing any exception.
	 * -------------------------------------------------------------------------
	 * ATTENTION: In case waiting (sleeping) threads are released without
	 * throwing an {@link Exception}, then this means that the using instances
	 * intends to shut down so that in such as case, no more calls to this
	 * method are to be performed (are to be prevented), such looping threads
	 * would lead to endless looping and cause consumption of calculation power.
	 * 
	 * @throws E Thrown when a next exception has been provided via
	 *         {@link #throwException(Exception)};
	 */
	@Override
	public void catchException() throws E {
		ControlFlowUtility.throwIllegalStateException( _isDisposed );
		synchronized ( this ) {
			waitForExceptionHandling();
			_threadCount.incrementAndGet();
			try {
				wait();
			}
			catch ( InterruptedException ie ) {}
		}
		final E theException = _exception;
		_threadCount.decrementAndGet();
		if ( theException == null ) {
			return;
		}
		if ( _threadCount.get() == 0 ) {
			_exception = null;
		}
		throw theException;
	}

	/**
	 * Returns the last exception which was pushed via
	 * {@link #throwException(Exception)}. THis might not be the same as being
	 * retrieved by {@link #catchException()}, as {@link #catchException()}
	 * might provide queued up {@link Exception} instances till all
	 * {@link Exception} instances have been processed.
	 * 
	 * @return The last exception being pushed via
	 *         {@link #throwException(Exception)};
	 */
	@Override
	public E lastException() {
		ControlFlowUtility.throwIllegalStateException( _isDisposed );
		return _lastException;
	}

	/**
	 * Passes an exception to the {@link ExceptionWatchdog} notifying all
	 * awaiting threads inside the {@link #catchException()} method to be
	 * provided with that {@link Exception}.
	 * 
	 * @param aException The {@link Exception} to be passed to the awaiting
	 *        threads inside the {@link #catchException()} method.
	 */
	public void throwException( E aException ) {
		ControlFlowUtility.throwIllegalStateException( _isDisposed );
		_exception = aException;
		synchronized ( this ) {
			notifyAll();
		}
		waitForExceptionHandling();
		_lastException = aException;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void releaseAll() {
		ControlFlowUtility.throwIllegalStateException( _isDisposed );
		_exception = null;
		synchronized ( this ) {
			notifyAll();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		_isDisposed = true;
		_exception = null;
		synchronized ( this ) {
			notifyAll();
			// In case we notified just before a thread entered the
			// #catchException() method. This can happen in a #catchException()
			// loop. That thread is notified now:
			try {
				wait( SleepLoopTime.MAX.getTimeMillis() );
			}
			catch ( InterruptedException ignored ) {}
			notifyAll();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Wait for last #throwException to finish.
	 */
	private void waitForExceptionHandling() {
		final long theStartTime = System.currentTimeMillis();
		while ( _threadCount.get() > 0 && _exception != null && System.currentTimeMillis() - theStartTime < SleepLoopTime.NORM.getTimeMillis() ) {
			try {
				Thread.sleep( SleepLoopTime.MIN.getTimeMillis() );
			}
			catch ( InterruptedException ignored ) {}
		}
	}
}
