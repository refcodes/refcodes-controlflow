// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.Arrays;

/**
 * This {@link DelegationInterceptorComposite} passes the work piece from one
 * {@link DelegationInterceptor} instance to the next in the order in which them
 * were added (passed). In case any of the contained {@link Interceptor}
 * instances flags the processing of the work piece to be "finished" (in this
 * case its {@link #intercept(Object)} method returns true), then this
 * {@link #intercept(Object)} method also returns true!
 *
 * @param <WP> The work piece which is being passed to the contained
 *        {@link Interceptor} instances one after the other for processing.
 */
public class DelegationInterceptorComposite<WP> extends AbstractInterceptorComposite<DelegationInterceptor<WP>> implements DelegationInterceptor<WP> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _isContinueOnFinished;
	private boolean _isContinueOnError;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTIR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link DelegationInterceptorComposite}.
	 */
	public DelegationInterceptorComposite() {
		this( false, false, (DelegationInterceptor<WP>[]) null );
	}

	/**
	 * Constructs a {@link DelegationInterceptorComposite}.
	 *
	 * @param isContinueOnFinished True in case all subsequent interceptors are
	 *        to be processed even if an interceptor signaled "finished" after
	 *        processing the work piece.
	 * @param isContinueOnError True in case all subsequent interceptors are to
	 *        be processed even if an interceptor signaled an error after
	 *        processing the work piece (by throwing an {@link Exception}).
	 */
	public DelegationInterceptorComposite( boolean isContinueOnFinished, boolean isContinueOnError ) {
		this( isContinueOnFinished, isContinueOnError, (DelegationInterceptor<WP>[]) null );
	}

	/**
	 * Constructs a {@link DelegationInterceptorComposite}.
	 * 
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public DelegationInterceptorComposite( DelegationInterceptor<WP>... aInterceptors ) {
		this( false, false, aInterceptors );
	}

	/**
	 * Constructs a {@link DelegationInterceptorComposite}.
	 *
	 * @param isContinueOnFinished True in case all subsequent interceptors are
	 *        to be processed even if an interceptor signaled "finished" after
	 *        processing the work piece.
	 * @param isContinueOnError True in case all subsequent interceptors are to
	 *        be processed even if an interceptor signaled an error after
	 *        processing the work piece (by throwing an {@link Exception}).
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public DelegationInterceptorComposite( boolean isContinueOnFinished, boolean isContinueOnError, DelegationInterceptor<WP>... aInterceptors ) {
		_isContinueOnFinished = isContinueOnFinished;
		_isContinueOnError = isContinueOnError;
		_interceptors = Arrays.asList( aInterceptors );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean intercept( WP aWorkPiece ) {
		boolean isFinished = false;
		for ( DelegationInterceptor<WP> eInterceptor : _interceptors ) {
			try {
				if ( eInterceptor.intercept( aWorkPiece ) ) {
					isFinished = true;
				}
				if ( !_isContinueOnFinished && isFinished ) {
					return true;
				}
			}
			catch ( Exception e ) {
				if ( !_isContinueOnError ) {
					throw e;
				}
			}
		}
		return isFinished;
	}
}