// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import org.refcodes.mixin.ReadTimeoutMillisAccessor;

/**
 * The {@link RetryTimeout} can be used in loops to test whether a timeout has
 * been reached, if the timeout has not been reached, then a given period of
 * time (delay) is waited. In case no more timeout is "left" the business logic
 * may act accordingly such as throwing an exception.
 * <p>
 * The actual timeout being waited is the effective time elapsed from the first
 * call to {@link #nextRetry()} till the last call to {@link #nextRetry()}. Any
 * delay inside the iteration loop is not added up to the delay.
 */
public class RetryTimeout implements Retryable, ReadTimeoutMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private long _currentTimeInMs = -1;
	private final long _timeoutInMs;
	private long _retryDelayInMs;
	private int _retryCount = 0;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link RetryTimeout} with the given timeout and the given
	 * retry delay. The retry number is round about:
	 * -------------------------------------------------------------------------
	 * "aTimeoutMillis / aRetryDelayInMs"
	 * -------------------------------------------------------------------------
	 * CAUTION: The above mentioned is not necessarily so, in case your business
	 * logic inside the loop iterations is slow, then less retries fit in the
	 * given timeout.
	 * 
	 * @param aTimeoutMillis The total time in milliseconds all iterations
	 *        together will delay.
	 * @param aRetryDelayInMs The delay before each retry to wait in
	 *        milliseconds.
	 */
	public RetryTimeout( long aTimeoutMillis, long aRetryDelayInMs ) {
		_timeoutInMs = aTimeoutMillis;
		_retryDelayInMs = aRetryDelayInMs;
	}

	/**
	 * Constructs the {@link RetryTimeout} with the given timeout and the given
	 * number of retry loops. The retry delay is round about:
	 * -------------------------------------------------------------------------
	 * "aTimeoutMillis / aRetryLoops"
	 * -------------------------------------------------------------------------
	 * CAUTION: The above mentioned is not necessarily so, in case your business
	 * logic inside the loop iterations is slow, then less retries fit in the
	 * given timeout.
	 * 
	 * @param aTimeoutMillis The total time in milliseconds all iterations
	 *        together will delay.
	 * @param aRetryLoops The number of retries, each retry delay before each
	 *        retry to wait in milliseconds is about the timeout divided by the
	 *        retry number.
	 */
	public RetryTimeout( long aTimeoutMillis, int aRetryLoops ) {
		_timeoutInMs = aTimeoutMillis;
		_retryDelayInMs = aTimeoutMillis / aRetryLoops;
		if ( _retryDelayInMs <= 0 ) {
			_retryDelayInMs = 1;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Tests whether a next retry is possible. In case this is the case, then
	 * the thread is delayed by the configured (implementation specific) delay
	 * time or until the provided monitor is notified via
	 * {@link Object#notify()} or {@link Object#notifyAll()}. Similar to the
	 * {@link #nextRetry()} with the difference of accepting a dedicated monitor
	 * which is used for aborting the dellay.
	 *
	 * @return True in case there is a next retry as of {@link #hasNextRetry()}.
	 */
	@Override
	public boolean nextRetry() {
		return nextRetry( this );
	}

	/**
	 * Tests whether a next retry is possible. In case this is the case, then
	 * the thread is delayed by the configured (implementation specific) delay
	 * time or until the provided monitor is notified via
	 * {@link Object#notify()} or {@link Object#notifyAll()}. Similar to the
	 * {@link #nextRetry()} with the difference of accepting a dedicated monitor
	 * which is used for aborting the dellay.
	 *
	 * @param aMonitor the monitor
	 * 
	 * @return True in case there is a next retry as of {@link #hasNextRetry()}.
	 */
	public boolean nextRetry( Object aMonitor ) {
		if ( _currentTimeInMs == -1 ) {
			_currentTimeInMs = System.currentTimeMillis();
		}
		if ( !hasNextRetry() ) {
			return false;
		}
		synchronized ( aMonitor ) {
			try {
				aMonitor.wait( _retryDelayInMs );
			}
			catch ( InterruptedException ignored ) {}
		}
		_retryCount++;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNextRetry() {
		return ( _currentTimeInMs == -1 ) || ( System.currentTimeMillis() < ( _currentTimeInMs + _timeoutInMs ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRetryCount() {
		return _retryCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void abort() {
		_currentTimeInMs = System.currentTimeMillis() - ( _timeoutInMs + 1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void restart() {
		_retryCount = 0;
		_currentTimeInMs = -1;
	}

	/**
	 * Returns the timeout altogether the {@link RetryTimeout} is waiting while
	 * iterating through the {@link RetryTimeout} via {@link #hasNextRetry()}
	 * and {@link #nextRetry()}. The actual timeout being waited is the
	 * effective time elapsed from the first call to {@link #nextRetry()} till
	 * the last call to {@link #nextRetry()}. Any delay inside the iteration
	 * loop is not added up to the delay.
	 * 
	 * @return The timeout in milliseconds.
	 */
	@Override
	public long getReadTimeoutMillis() {
		return _timeoutInMs;
	}

	/**
	 * Returns retry to wait in milliseconds upon calling {@link #nextRetry()}).
	 * 
	 * @return The delay being set in milliseconds.
	 */
	public long getRetryDelayInMs() {
		return _retryDelayInMs;
	}
}
