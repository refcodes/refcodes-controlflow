// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

/**
 * The {@link Interceptable} provides base functionality for working with
 * {@link Interceptor} instances.
 *
 * @param <I> The type of {@link Interceptor} to be managed.
 */
public interface Interceptable<I extends Interceptor<?, ?>> {

	/**
	 * Tests whether the given {@link Interceptor} instance has been added.
	 * 
	 * @param aInterceptor The {@link Interceptor} instance for which to test if
	 *        it has been added.
	 * 
	 * @return True if the given {@link Interceptor} instance has been added
	 *         already.
	 */
	boolean hasInterceptor( I aInterceptor );

	/**
	 * Adds the given {@link Interceptor} instance. The {@link Interceptor}
	 * instance itself acts as the handle which is used when removing the given
	 * {@link Interceptor} instance later.
	 * 
	 * @param aInterceptor The {@link Interceptor} instance which is to be
	 *        added.
	 * 
	 * @return True if the {@link Interceptor} instance has been added
	 *         successfully. If the {@link Interceptor} instance has already
	 *         been added, false is returned.
	 */
	boolean addInterceptor( I aInterceptor );

	/**
	 * Removes the {@link Interceptor} instance. In case the {@link Interceptor}
	 * instance has not been added before, then false is returned.
	 * 
	 * @param aInterceptor The {@link Interceptor} instance which is to be
	 *        removed.
	 * 
	 * @return True if the {@link Interceptor} instance has been removed
	 *         successfully. If there was none such {@link Interceptor} instance
	 *         or if the {@link Interceptor} instance has already been removed,
	 *         then false is returned.
	 */
	boolean removeInterceptor( I aInterceptor );

}