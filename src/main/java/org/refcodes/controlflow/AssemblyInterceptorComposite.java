// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This {@link AssemblyInterceptorComposite} passes the work piece from one
 * {@link AssemblyInterceptor} instance to the next in the order in which them
 * were added (passed).An {@link AssemblyInterceptor} assembles a work piece
 * object whilst the work piece is passed to the next
 * {@link AssemblyInterceptor} in the assembly line till all
 * {@link AssemblyInterceptor} in the assembly line were invoked.
 *
 * @param <WP> The work piece which is being passed to the contained
 *        {@link Interceptor} instances one after the other for processing.
 */
public class AssemblyInterceptorComposite<WP> extends AbstractInterceptorComposite<AssemblyInterceptor<WP>> implements AssemblyInterceptor<WP> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _isContinueOnError;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTIR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AssemblyInterceptorComposite}.
	 */
	public AssemblyInterceptorComposite() {
		this( false, (AssemblyInterceptor<WP>[]) null );
	}

	/**
	 * Constructs the {@link AssemblyInterceptorComposite}.
	 * 
	 * @param isContinueOnError True in case all subsequent
	 *        {@link AssemblyInterceptorComposite} instances are to be processed
	 *        even if an {@link AssemblyInterceptorComposite} instance signaled
	 *        an error after processing the work piece (by throwing an
	 *        {@link Exception}).
	 */
	public AssemblyInterceptorComposite( boolean isContinueOnError ) {
		this( isContinueOnError, (AssemblyInterceptor<WP>[]) null );
	}

	/**
	 * Constructs the {@link AssemblyInterceptorComposite}.
	 * 
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public AssemblyInterceptorComposite( AssemblyInterceptor<WP>... aInterceptors ) {
		this( false, aInterceptors );
	}

	/**
	 * CConstructs the {@link AssemblyInterceptorComposite}.
	 * 
	 * @param isContinueOnError True in case all subsequent interceptors are to
	 *        be processed even if an interceptor signaled an error after
	 *        processing the work piece (by throwing an {@link Exception}).
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public AssemblyInterceptorComposite( boolean isContinueOnError, AssemblyInterceptor<WP>... aInterceptors ) {
		_isContinueOnError = isContinueOnError;
		_interceptors = aInterceptors == null ? new ArrayList<>() : new ArrayList<>( Arrays.asList( aInterceptors ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WP intercept( WP aWorkPiece ) {
		if ( aWorkPiece == null ) {
			throw new NullPointerException( "The provided work piece must not be <null>!" );
		}
		for ( AssemblyInterceptor<WP> eInterceptor : _interceptors ) {
			try {
				aWorkPiece = eInterceptor.intercept( aWorkPiece );
				if ( aWorkPiece == null ) {
					throw new NullPointerException( "The intercepted work piece must not be <null>!" );
				}
			}
			catch ( Exception e ) {
				if ( !_isContinueOnError ) {
					throw e;
				}
			}
		}
		return aWorkPiece;
	}
}