module org.refcodes.controlflow {
	requires org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;

	exports org.refcodes.controlflow;
}
