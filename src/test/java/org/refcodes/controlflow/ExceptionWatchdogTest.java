// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import static org.junit.jupiter.api.Assertions.*;
import java.lang.Thread.State;
import org.junit.jupiter.api.Test;

public class ExceptionWatchdogTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSingleExceptionWatchdog() {
		final ExceptionWatchdog<Exception> theExceptionWatchdog = new ExceptionWatchdog<>();
		final AwaitExceptionThread t1 = new AwaitExceptionThread( theExceptionWatchdog );
		t1.start();
		pause( 100 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Calling #nextException( ... )" );
		}
		theExceptionWatchdog.throwException( new Exception( "Catch me if you can!" ) );
		pause( 100 );
		theExceptionWatchdog.releaseAll();
		pause( 100 );
		assertEquals( 1, t1.getExceptionCount() );
		pause( 100 );
		assertFalse( t1.isAlive() );
	}

	@Test
	public void testTwoExceptionWatchdog() {
		final ExceptionWatchdog<Exception> theExceptionWatchdog = new ExceptionWatchdog<>();
		final AwaitExceptionThread t1 = new AwaitExceptionThread( theExceptionWatchdog );
		t1.start();
		pause( 100 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Calling #nextException( ... )" );
		}
		theExceptionWatchdog.throwException( new Exception( "Catch me if you can!" ) );
		pause( 100 );
		theExceptionWatchdog.throwException( new Exception( "Catch as catch can!" ) );
		pause( 100 );
		theExceptionWatchdog.releaseAll();
		assertEquals( t1.getExceptionCount(), 2 );
		pause( 100 );
		assertFalse( t1.isAlive() );
	}

	@Test
	public void testMultiExceptionWatchdog() {
		final ExceptionWatchdog<Exception> theExceptionWatchdog = new ExceptionWatchdog<>();
		final AwaitExceptionThread[] t = new AwaitExceptionThread[10];
		for ( int i = 0; i < 10; i++ ) {
			t[i] = new AwaitExceptionThread( theExceptionWatchdog );
			t[i].start();
		}
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Calling #throwException( ... )" );
		}
		waitForThreads( t );
		theExceptionWatchdog.throwException( new Exception( "Catch me if you can!" ) );
		waitForThreads( t );
		theExceptionWatchdog.throwException( new Exception( "Catch as catch can!" ) );
		waitForThreads( t );
		theExceptionWatchdog.releaseAll();
		for ( AwaitExceptionThread aT : t ) {
			assertEquals( 2, aT.getExceptionCount() );
		}
		pause( 500 );
		for ( AwaitExceptionThread aT : t ) {
			assertFalse( aT.isAlive() );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void waitForThreads( AwaitExceptionThread[] t ) {
		for ( int i = 0; i < 10; i++ ) {
			while ( t[i].getState() != State.WAITING ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( t[i].getName() + " = " + t[i].getState() );
				}
				pause( 100 );
			}
		}
	}

	private void pause( int aTimeout ) {
		try {
			Thread.sleep( aTimeout );
		}
		catch ( InterruptedException ignored ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class AwaitExceptionThread extends Thread {
		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final ExceptionWatchdog<Exception> _exceptionWatchdog;
		private int _exceptionCount = 0;
		private boolean _isStarted = false;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		public AwaitExceptionThread( ExceptionWatchdog<Exception> aExceptionWatchdog ) {
			_exceptionWatchdog = aExceptionWatchdog;
		}
		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		@Override
		public void start() {
			super.start();
			final RetryCounter theRetry = new RetryCounter( 10, 100 );
			while ( theRetry.hasNextRetry() && !_isStarted ) {
				theRetry.nextRetry();
			}
		}

		@Override
		public void run() {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Thread [" + Thread.currentThread().getName() + "]: Thread started ..." );
			}
			Exception theException;
			do {
				theException = null;
				try {
					_isStarted = true;
					if ( IS_LOG_TESTS_ENABLED ) {
						System.out.println( "Thread [" + Thread.currentThread().getName() + "]: PRE-catch ..." );
					}
					_exceptionWatchdog.catchException();
				}
				catch ( Exception e ) {
					if ( IS_LOG_TESTS_ENABLED ) {
						System.out.println( "Thread [" + Thread.currentThread().getName() + "]: POST-catch." );
					}
					theException = e;
					_exceptionCount++;
					if ( IS_LOG_TESTS_ENABLED ) {
						System.out.println( "Thread [" + Thread.currentThread().getName() + "]: Caught an exception \"" + e.getMessage() + "\" <" + _exceptionCount + ">." );
					}
				}
			} while ( theException != null );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Thread [" + Thread.currentThread().getName() + "]: Thread released <" + _exceptionCount + ">." );
			}
		}

		public int getExceptionCount() {
			return _exceptionCount;
		}
	}
}
