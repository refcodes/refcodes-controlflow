// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

public class AssemblyInterceptorTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAssembleFibonacci() {
		final int[] theExpected = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };
		final AssemblyInterceptorComposite<List<Integer>> theAssemblyLine = new AssemblyInterceptorComposite<>();
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 0
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 1
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 1
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 2
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 3
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 5
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 8
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 13
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 21
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 34
		theAssemblyLine.addInterceptor( AssemblyInterceptorTest::addFibonacci ); // 55
		List<Integer> theFibonaccis = new ArrayList<>();
		theFibonaccis = theAssemblyLine.intercept( theFibonaccis );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theFibonaccis );
		}
		for ( int i = 0; i < theFibonaccis.size(); i++ ) {
			assertEquals( theExpected[i], theFibonaccis.get( i ) );
		}
		assertEquals( theExpected.length, theFibonaccis.size() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static List<Integer> addFibonacci( List<Integer> aFibonaccis ) {
		if ( aFibonaccis.size() == 0 ) {
			aFibonaccis.add( 0 );
		}
		else if ( aFibonaccis.size() < 2 ) {
			aFibonaccis.add( 1 );
		}
		else {
			aFibonaccis.add( aFibonaccis.get( aFibonaccis.size() - 2 ) + aFibonaccis.get( aFibonaccis.size() - 1 ) );
		}
		return aFibonaccis;
	}
}
