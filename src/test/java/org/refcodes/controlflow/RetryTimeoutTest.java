// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class RetryTimeoutTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long RETRY_DELAY_IN_MS = 200;
	private static final long TIMEOUT_IN_MS = 2000;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRetryTimeout() {
		final RetryTimeout theRetryTimeout = new RetryTimeout( TIMEOUT_IN_MS, RETRY_DELAY_IN_MS );
		final long theStartTime = System.currentTimeMillis();
		while ( theRetryTimeout.hasNextRetry() ) {
			theRetryTimeout.nextRetry();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( ( System.currentTimeMillis() / 1000 ) + " [" + theRetryTimeout.getRetryCount() + "]" );
			}
		}
		final long theEndTime = System.currentTimeMillis();
		assertTrue( theEndTime - theStartTime >= TIMEOUT_IN_MS );
		assertTrue( theEndTime - theStartTime < TIMEOUT_IN_MS + RETRY_DELAY_IN_MS );
	}

	@Test
	public void testRainyDayRetryTimeout() {
		final RetryTimeout theRetryTimeout = new RetryTimeout( TIMEOUT_IN_MS, RETRY_DELAY_IN_MS );
		final long theStartTime = System.currentTimeMillis();
		while ( theRetryTimeout.hasNextRetry() ) {
			theRetryTimeout.nextRetry();
			synchronized ( this ) {
				try {
					Thread.sleep( TIMEOUT_IN_MS );
				}
				catch ( InterruptedException ignored ) {}
			}
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( ( System.currentTimeMillis() / 1000 ) + " [" + theRetryTimeout.getRetryCount() + "]" );
			}
		}
		final long theEndTime = System.currentTimeMillis();
		assertEquals( 1, theRetryTimeout.getRetryCount() );
		assertTrue( theEndTime - theStartTime < TIMEOUT_IN_MS + ( RETRY_DELAY_IN_MS * 2 ) );
	}
}
