// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class DelegationInterceptorTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testExitOnAll() {
		final AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		final Result theResult = new Result();
		final DelegationInterceptorComposite<Result> theCompoundSequentialInterceptor = new DelegationInterceptorComposite<>( false, false, theAddInterceptor, theAddInterceptor, theAddInterceptor );
		theCompoundSequentialInterceptor.intercept( theResult );
		assertEquals( 2, theResult.getResult() );
	}

	@Test
	public void testContinueOnFinished() {
		final AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		final Result theResult = new Result();
		final DelegationInterceptorComposite<Result> theCompoundSequentialInterceptor = new DelegationInterceptorComposite<>( true, false, theAddInterceptor, theAddInterceptor, theAddInterceptor );
		theCompoundSequentialInterceptor.intercept( theResult );
		assertEquals( 6, theResult.getResult() );
	}

	@Test
	public void testFaliOnError() {
		final AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		final DivInterceptor theDivInterceptor = new DivInterceptor( 0 );
		final Result theResult = new Result();
		final DelegationInterceptorComposite<Result> theCompoundSequentialInterceptor = new DelegationInterceptorComposite<>( false, false, theDivInterceptor, theAddInterceptor, theAddInterceptor, theAddInterceptor );
		try {
			theCompoundSequentialInterceptor.intercept( theResult );
			fail( "Should not reach this code!" );
		}
		catch ( Exception e ) {
			/* expected */
		}
	}

	@Test
	public void testContinueOnAll() {
		final AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		final DivInterceptor theDivInterceptor = new DivInterceptor( 0 );
		final Result theResult = new Result();
		final DelegationInterceptorComposite<Result> theCompoundSequentialInterceptor = new DelegationInterceptorComposite<>( true, true, theAddInterceptor, theDivInterceptor, theAddInterceptor, theAddInterceptor );
		theCompoundSequentialInterceptor.intercept( theResult );
		assertEquals( 6, theResult.getResult() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public class AddInterceptor implements DelegationInterceptor<Result> {
		private final int _summand;

		public AddInterceptor( int aSummand ) {
			_summand = aSummand;
		}

		@Override
		public Boolean intercept( Result aWorkPiece ) {
			aWorkPiece.setResult( aWorkPiece.getResult() + _summand );
			return true;
		}
	}

	public class DivInterceptor implements DelegationInterceptor<Result> {
		private final int _divisor;

		public DivInterceptor( int aDivisor ) {
			_divisor = aDivisor;
		}

		@Override
		public Boolean intercept( Result aWorkPiece ) {
			aWorkPiece.setResult( aWorkPiece.getResult() / _divisor );
			return true;
		}
	}

	public class Result {
		private int _result = 0;

		public int getResult() {
			return _result;
		}

		public void setResult( int aResult ) {
			_result = aResult;
		}
	}
}
