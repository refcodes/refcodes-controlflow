// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.data.LoopExtensionTime;

public class RetryCounterTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );
	private static final int RETRY_DELAY_MILLISECONDS = 300;
	private static final int RETRY_NUMBER = 5;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRetryCounter() {
		final RetryCounter theRetryCounter = new RetryCounter( RETRY_NUMBER, RETRY_DELAY_MILLISECONDS );
		int theCounter = 0;
		long eTime = System.currentTimeMillis();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "STARTING WITH  := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
		}
		while ( theRetryCounter.nextRetry() ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "hasNextRetry() := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
			}
			if ( theRetryCounter.getRetryCount() == 1 ) {
				// Expecting no delay upon first loop:
				assertTrue( System.currentTimeMillis() - eTime < RETRY_DELAY_MILLISECONDS );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [no delay]" );
				}
			}
			else {
				// Expecting delay upon succeeding loops:
				final long a = System.currentTimeMillis();
				final long b = a - eTime;
				final long c = theRetryCounter.getCurrentRetryDelayMillis();
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "a := System.currentTimeMillis()" );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "b := a - eTime" );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "c := theRetryCounter.getCurrentRetryDelayInMs()" );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "a := " + a );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "b := " + b );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "c := " + c );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "eTime := " + eTime );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "retryCount := " + theRetryCounter.getRetryCount() );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "retryNumber := " + theRetryCounter.getRetryNumber() );
				}
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "b >= c ???" );
				}
				assertTrue( b >= c );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [delay]" );
				}
			}
			eTime = System.currentTimeMillis();
			theCounter++;
		}
		// @formatter:off
		if ( IS_LOG_TESTS_ENABLED)
		 {
			System.out.println( "ENDING WITH    := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
			// @formatter:on
		}
		// Expecting no delay when exiting the loop:
		assertTrue( System.currentTimeMillis() - eTime <= RETRY_DELAY_MILLISECONDS );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( " [no delay]" );
		}
		assertEquals( RETRY_NUMBER, theCounter );
	}

	@Test
	public void testExponentialRetryCounter() {
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "##### EXPONENTIAL RETRY COUNTER #####" );
		}
		final RetryCounter theRetryCounter = new RetryCounter( RETRY_NUMBER, RETRY_DELAY_MILLISECONDS, LoopExtensionTime.MIN.getTimeMillis() );
		int theCounter = 0;
		long eTime = System.currentTimeMillis();
		long currentTimeMillis;
		// @formatter:off
		if ( IS_LOG_TESTS_ENABLED) {
			System.out.println( "STARTING WITH  := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
		}
		// @formatter:off
			while ( theRetryCounter.nextRetry() ) {
			// @formatter:off
			if ( IS_LOG_TESTS_ENABLED)
			 {
				System.out.println( "hasNextRetry() := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
				// @formatter:on
			}
			if ( theRetryCounter.getRetryCount() == 1 ) {
				// Expecting no delay upon first loop:
				currentTimeMillis = System.currentTimeMillis();
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "Before-Time := <" + eTime + ">, After-Time := <" + currentTimeMillis + ">, Wait-Time := <" + ( currentTimeMillis - eTime ) + ">, expected Wait-Time less than := <" + RETRY_DELAY_MILLISECONDS + ">." );
				}
				assertTrue( currentTimeMillis - eTime < RETRY_DELAY_MILLISECONDS );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [no delay]" );
				}
			}
			else {
				// Expecting delay upon succeeding loops:
				currentTimeMillis = System.currentTimeMillis();
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "Before-Time := <" + eTime + ">, After-Time := <" + currentTimeMillis + ">, Wait-Time := <" + ( currentTimeMillis - eTime ) + ">, expected Wait-Time := <" + RETRY_DELAY_MILLISECONDS + ">." );
				}
				assertTrue( currentTimeMillis - eTime >= RETRY_DELAY_MILLISECONDS );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [delay]" );
				}
			}
			eTime = System.currentTimeMillis();
			theCounter++;
		}
		// @formatter:off
		if ( IS_LOG_TESTS_ENABLED)
		 {
			System.out.println( "ENDING WITH    := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
			// @formatter:on
		}
		// Expecting no delay when exiting the loop:
		assertTrue( System.currentTimeMillis() - eTime < RETRY_DELAY_MILLISECONDS );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( " [no delay]" );
		}
		assertEquals( RETRY_NUMBER, theCounter );
	}

	@Test
	public void testOneRetryCounter() {
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "##### ONE RETRY COUNTER #####" );
		}
		final RetryCounter theRetryCounter = new RetryCounter( 1, RETRY_DELAY_MILLISECONDS, LoopExtensionTime.MIN.getTimeMillis() );
		int theCounter = 0;
		long eTime = System.currentTimeMillis();
		// @formatter:off
		if ( IS_LOG_TESTS_ENABLED) {
			System.out.println( "STARTING WITH  := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
		}
		// @formatter:off
			while ( theRetryCounter.nextRetry() ) {
					// @formatter:off
			if ( IS_LOG_TESTS_ENABLED)
			 {
				System.out.println( "hasNextRetry() := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
				// @formatter:on
			}
			if ( theRetryCounter.getRetryCount() == 1 ) {
				// Expecting no delay upon first loop:
				assertTrue( System.currentTimeMillis() - eTime < RETRY_DELAY_MILLISECONDS );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [no delay]" );
				}
			}
			else {
				// Expecting delay upon succeeding loops:
				assertTrue( System.currentTimeMillis() - eTime >= RETRY_DELAY_MILLISECONDS );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [delay]" );
				}
			}
			eTime = System.currentTimeMillis();
			theCounter++;
		}
		// @formatter:off
		if ( IS_LOG_TESTS_ENABLED)
		 {
			System.out.println( "ENDING WITH    := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
			// @formatter:on
		}
		// Expecting no delay when exiting the loop:
		assertTrue( System.currentTimeMillis() - eTime < RETRY_DELAY_MILLISECONDS );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( " [no delay]" );
		}
		assertEquals( 1, theCounter );
	}

	@Test
	public void testNoneRetryCounter() {
		final RetryCounter theRetryCounter = new RetryCounter( 0, RETRY_DELAY_MILLISECONDS, LoopExtensionTime.MIN.getTimeMillis() );
		int theCounter = 0;
		long eTime = System.currentTimeMillis();
		// @formatter:off
		if ( IS_LOG_TESTS_ENABLED) {
			System.out.println( "STARTING WITH  := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
		}
		// @formatter:off
			while ( theRetryCounter.nextRetry() ) {
			// @formatter:off
			if ( IS_LOG_TESTS_ENABLED)
			 {
				System.out.println( "hasNextRetry() := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
				// @formatter:on
			}
			if ( theRetryCounter.getRetryCount() == 1 ) {
				// Expecting no delay upon first loop:
				assertTrue( System.currentTimeMillis() - eTime < RETRY_DELAY_MILLISECONDS );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [no delay]" );
				}
			}
			else {
				// Expecting delay upon succeeding loops:
				assertTrue( System.currentTimeMillis() - eTime >= RETRY_DELAY_MILLISECONDS );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " [delay]" );
				}
			}
			eTime = System.currentTimeMillis();
			theCounter++;
		}
		// @formatter:off
		if ( IS_LOG_TESTS_ENABLED)
		 {
			System.out.println( "ENDING WITH    := " + eTime + " (before) - " + theRetryCounter.getRetryNumber() + " (retry number) - " + theRetryCounter.getRetryCount() + " (retry count) - " + theRetryCounter.getInitialRetryDelayMillis() + " (delay) - " + theRetryCounter.getCurrentRetryDelayMillis() + " (current delay) - " + theRetryCounter.getNextRetryDelayMillis() + " (next delay) - " + System.currentTimeMillis() + " (after)" );
			// @formatter:on
		}
		// Expecting no delay when exiting the loop:
		assertTrue( System.currentTimeMillis() - eTime < RETRY_DELAY_MILLISECONDS );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( " [no delay]" );
		}
		assertEquals( 0, theCounter );
	}

	@Test
	public void testRetryCounterExit() {
		final RetryCounter theRetryCounter = new RetryCounter( RETRY_NUMBER, RETRY_DELAY_MILLISECONDS, LoopExtensionTime.MIN.getTimeMillis() );
		int theCounter = 0;
		final long theTime = System.currentTimeMillis();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Start time = " + theTime );
		}
		while ( theRetryCounter.nextRetry() ) {
			theRetryCounter.abort();
			assertEquals( 0, theCounter );
			if ( theRetryCounter.hasNextRetry() ) {
				fail( "Should not reach this code!" );
			}
			theCounter++;
		}
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "End time   = " + System.currentTimeMillis() );
		}
		assertTrue( System.currentTimeMillis() - theTime < RETRY_DELAY_MILLISECONDS );
	}
}
