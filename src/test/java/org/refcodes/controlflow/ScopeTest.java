// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.controlflow;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class ScopeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Word _wordChain = new Word( "0", new Word( "1", new Word( "2", new Word( "3", new Word( "4", new Word( "5", new Word( "6", new Word( "7", new Word( "8", new Word( "9" ) ) ) ) ) ) ) ) ) );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////F

	@Test
	public void testSentence1() {
		final Scope theScope = new Scope( 0, 10 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "0123456789", theSentence );
	}

	@Test
	public void testSentence2() {
		final Scope theScope = new Scope( 0, 5 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "01234", theSentence );
	}

	@Test
	public void testSentence3() {
		final Scope theScope = new Scope( 5, 5 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "56789", theSentence );
	}

	@Test
	public void testSentence4() {
		final Scope theScope = new Scope( 0, 0 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "", theSentence );
	}

	@Test
	public void testSentence5() {
		final Scope theScope = new Scope( 0, 11 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "0123456789", theSentence );
	}

	@Test
	public void testSentence6() {
		final Scope theScope = new Scope( 13, 25 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "", theSentence );
	}

	@Test
	public void testSentence7() {
		final Scope theScope = new Scope( 1 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "123456789", theSentence );
	}

	@Test
	public void testSentence8() {
		final Scope theScope = new Scope( 5 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "56789", theSentence );
	}

	@Test
	public void testSentence9() {
		final Scope theScope = new Scope( 13 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "", theSentence );
	}

	@Test
	public void testSentence10() {
		final Scope theScope = new Scope( 0, 0 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theScope );
			System.out.println( _wordChain );
		}
		final String theSentence = _wordChain.toSentence( theScope );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "==> \"" + theSentence + "\"" );
		}
		assertEquals( "", theSentence );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Word {
		Word _successor;
		String _word;

		public Word( String aWord, Word aSuccessor ) {
			_successor = aSuccessor;
			_word = aWord;
		}

		public Word( String aWord ) {
			_successor = null;
			_word = aWord;
		}

		public String toSentence( Coupler aCoupler ) {
			String theWord = "";
			if ( aCoupler.isValid() ) {
				theWord = _word;
			}
			theWord += aCoupler.invokeNextOr( _successor, Word::toSentence, "" );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( aCoupler.isValid() ) {
					System.out.println( "--> \"" + theWord + "\" (+" + _word + ")" );
				}
				else {
					System.out.println( "--> \"" + theWord + "\" (!" + _word + ")" );
				}
			}
			return theWord;
		}

		@Override
		public String toString() {
			return _word + ( _successor != null ? " " + _successor.toString() : "" );
		}
	}
}
